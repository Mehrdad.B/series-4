package sbu.cs.parser.json;

public class JsonBool extends JsonBasic{
    private boolean value;
    public void setValue(boolean value)
    {
        this.value=value;
    }
    @Override
    public String getValue()
    {
        return Boolean.toString(value);
    }
}
