package sbu.cs.parser.json;

public class JsonDouble extends JsonBasic{
    private double value;
    public void setValue(double value)
    {
        this.value=value;
    }
    @Override
    public String getValue()
    {
        return Double.toString(value);
    }
}
