package sbu.cs.parser.json;

public class JsonInt extends JsonBasic{
    private int value;
    public void setValue(int value)
    {
        this.value=value;
    }
    @Override
    public String getValue()
    {
        return Integer.toString(value);
    }
}
