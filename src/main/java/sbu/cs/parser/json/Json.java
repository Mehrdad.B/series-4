package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface
{
    List<JsonBasic> Elements ;

    public Json(List<JsonBasic> Elements) {
        this.Elements = Elements;
    }

    @Override
    public String getStringValue(String key)
    {
        for(int i=0 ; i<(Elements.size()) ; i++)
        {
            if(Elements.get(i).getKey().equals(key))
            {
                return Elements.get(i).getValue();
            }
        }
        return null;
    }
}
