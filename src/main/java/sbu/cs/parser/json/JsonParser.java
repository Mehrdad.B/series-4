package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class JsonParser
{
    static List<JsonBasic> Elements = new ArrayList<>();
    public static Json parse(String data)
    {
        String key;
        String value;
        data=data.replaceAll("(\\{|\"|\\s|})" , "");
        String temp = data;
        while(temp.indexOf("["  ) >=0 )
        {
            int begin = temp.indexOf("[");
            int end = temp.indexOf("]")+1;
            String arraycheck = temp.substring(begin , end);
            String changearray =  arraycheck.replaceAll("," , "-");
            data = data.replace(arraycheck , changearray );
            temp = temp.replace(arraycheck , "");
        }
        String[] keyvalue = data.split(",");
        seperate(keyvalue);
        Json json = new Json(Elements);
        return json;
    }
    public static void seperate(String[] keyvalue)
    {
        for(int i=0 ; i<keyvalue.length ; i++)
        {
            String[] str = keyvalue[i].split(":");
            if(str[1].matches("[0-9]*"))
            {
                JsonInt jsonInt = new JsonInt();
                jsonInt.setValue(Integer.parseInt(str[1]));
                jsonInt.setKey(str[0]);
                Elements.add(jsonInt);
            }
            else if(str[1].matches("[0-9]*\\.[0-9]*"))
            {
                JsonDouble jsonDouble = new JsonDouble();
                jsonDouble.setValue(Double.parseDouble(str[1]));
                jsonDouble.setKey(str[0]);
                Elements.add(jsonDouble);
            }
            else if(str[1].matches("(true|false)"))
            {
                JsonBool jsonBool = new JsonBool();
                jsonBool.setValue(Boolean.parseBoolean(str[1]));
                jsonBool.setKey(str[0]);
                Elements.add(jsonBool);
            }
            else
            {
                JsonString jsonString = new JsonString();
                jsonString.setValue((str[1]));
                jsonString.setKey(str[0]);
                Elements.add(jsonString);
            }
        }
    }
}
