package sbu.cs.parser.html;

public class HTMLParser
{

    public static Node parse(String document)
    {
        Node node = new Node(document);
        document = document.replaceAll("\n  " , "");
        document = opentag(document);
        while(document.contains("<"))
        {
            node.setTags(retruntag(document));
            document = deletetag(document);
        }
        return node;
    }

    public static String opentag(String document)
    {
        if(atributeintag(document))
        {
            String ftagname = "<" + retruntagname(document) + ">";
            int f = document.indexOf("<")+1;
            int l = document.indexOf(" ");
            String ltagname = "</" + document.substring(f , l) + ">";
            String tmp = document.replaceAll(ftagname , "").replaceAll(ltagname , "");
            return tmp;
        }
        else
        {
            String ftagname = "<" + retruntagname(document) + ">";
            String ltagname = "</" + retruntagname(document) + ">";
            String tmp = document.replaceAll(ftagname , "").replaceAll(ltagname , "");
            return tmp;
        }
    }

    public static Node retruntag(String document)
    {
        if(!isclosed(document))
        {
            int ftag = document.indexOf("<");
            int ltag = document.indexOf(">");
            String tmp = document.substring(ftag , ltag+1);
            Node node = new Node(tmp);
            return node;
        }
        else
        {
            if(atributeintag(document))
            {
                String ftagname = "<" + retruntagname(document) + ">";
                int ktag = document.indexOf("<")+1;
                int stag = document.indexOf(" ");
                String ltagname = document.substring(ktag , stag);
                ltagname = "</" + ltagname + ">";
                int ftag = document.indexOf(ftagname);
                int ltag = document.indexOf(ltagname);
                String tmp = document.substring(ftag , ltag+ltagname.length());
                Node node = new Node(tmp);
                return node;
            }
            else
            {
                String ftagname = "<" + retruntagname(document) + ">";
                String ltagname = "</" + retruntagname(document) + ">";
                int ftag = document.indexOf(ftagname);
                int ltag = document.indexOf(ltagname);
                String tmp = document.substring(ftag , ltag+ltagname.length());
                Node node = new Node(tmp);
                return node;
            }
        }
    }

    public static String deletetag(String document)
    {
        String Tag = retruntag(document).getTags();
        if(Tag.matches("</[a-z]+>"))
        {
            return document.substring(Tag.length());
        }
        return document.replaceAll(Tag , "");
    }

    public static String retruntagname(String document)
    {
        int ftag = document.indexOf("<")+1;
        int ltag = document.indexOf(">");
        return document.substring(ftag , ltag);
    }

    public static boolean isclosed(String document)
    {
        boolean flag = true;
        if((document.indexOf("/")-document.indexOf("<"))==1)
        {
            flag = false;
            return flag;
        }
        return flag;
    }

    public static boolean atributeintag(String document)
    {
        int ftag = document.indexOf("<")+1;
        int ltag = document.indexOf(">");
        if(document.substring(ftag , ltag).contains("="))
        {
            return true;
        }
         return false;
    }

}

