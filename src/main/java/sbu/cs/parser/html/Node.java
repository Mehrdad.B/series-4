package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {

    private List<Node> Tags = new ArrayList<>();
    private String Btag;
    private Map<String , String> keyvalue = new HashMap<>();
    public Node(String Btag)
    {
        this.Btag=Btag;
    }
    public void setTags(Node tag)
    {
        Tags.add(tag);
    }
    public String getTags()
    {
        return Btag;
    }

    @Override
    public String getStringInside()
    {
        if(!HTMLParser.isclosed(Btag))
        {
            return null;
        }
        return HTMLParser.opentag(Btag);
    }

    @Override
    public List<Node> getChildren()
    {
        for(int i=0 ; i<Tags.size() ; i++)
        {
            Node tag= HTMLParser.parse(Tags.get(i).Btag);
            Tags.set(i , tag);
        }
        return Tags;
    }

    @Override
    public String getAttributeValue(String key)
    {
        int ftag = Btag.indexOf(" ");
        int ltag = Btag.indexOf(">");
        String tmp = Btag.substring(ftag+1 , ltag);
        tmp = tmp.replaceAll("\" " , "@");
//        tmp = tmp.replaceAll("\\s" , "=");
        String[] keyvalues = tmp.split("@");
        for(int i=0 ; i<keyvalues.length ; i++)
        {
            String[] seperate = keyvalues[i].split("=");
            keyvalue.put(seperate[0].replaceAll("\"" , "") , seperate[1].replaceAll("\"" , ""));
        }
        return keyvalue.get(key);
    }
}
